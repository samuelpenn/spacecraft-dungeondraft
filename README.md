# Spacecraft Dungeondraft

SciFi assets for use in Dungeondraft.

This asset pack contains a collection of full sized spacecraft tokens. In
Traveller terms, these are 100t+ craft. They are mostly of use with the
Starport asset pack, but are kept seperate due to their size.

Each object is 10s of squares in size at the usual Traveller scale (1sq = 1.5m).
The tokens are external views of the ships, so they can be placed as docked
or landed craft on a map. They are of no use if you are inside one of the craft.

Smallcraft (under 100t) are part of the Smallcraft asset pack.

Vehicles are part of the Vehicles asset pack.

